

'''
https://packaging.python.org/en/latest/guides/distributing-packages-using-setuptools/#create-an-account

#
#	apt install python3.10-venv
#	pip install twine
#
'''

'''
	# poetry export --with dev --format requirements.txt --output requirements.txt
'''

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_directory = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_directory, path)))

add_paths_to_system ([
	'modules_pip'
])

