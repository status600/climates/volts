






'''
	python3 /biotech/venues/warehouse/0.0_solid_example/start.proc.py
'''

# cd /biotech/venues/warehouse/0_example


import pathlib
from os.path import dirname, join, normpath
import sys
def add_paths_to_system (paths):
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../stages_pip',
	'../../stages'
])

#
import pathlib
from os.path import dirname, join, normpath
#
import biotech
from biotech.topics.show.variable import show_variable

#



this_directory = pathlib.Path (__file__).parent.resolve ()
this_module = str (this_directory)


scan = biotech.on ({
	"glob_string": this_module + "/monitors/**/status_*.py",
	
	"simultaneous": True,
	"simultaneous_capacity": 10,

	"module_paths": [
		#normpath (join (this_module, "modules/estates")),
		#normpath (join (this_module, "modules/estates_2"))
	],

	"relative_path": this_module,
	
	"aggregation_format": 2
})

show_variable ({
	"scan:": scan ["proceeds"]
})

scan ["off"] ()