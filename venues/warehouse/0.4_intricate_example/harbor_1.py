from http.server import BaseHTTPRequestHandler, HTTPServer
import json
import threading
import time

class RequestHandler(BaseHTTPRequestHandler):
	def do_GET(self):
		self.send_response(200)
		self.send_header('Content-type', 'text/html')
		self.end_headers()
		self.wfile.write(b"Hello, World!")

	def do_PATCH(self):
		content_length = int(self.headers['Content-Length'])
		post_data = self.rfile.read(content_length)
		try:
			data = json.loads(post_data.decode('utf-8'))
			print("Received JSON data:", data)
			self.send_response(200)
		except json.JSONDecodeError:
			self.send_response(400)
		self.end_headers()

class Server:
	def __init__(self, host='0.0.0.0', port=5000):
		self.host = host
		self.port = port
		self.httpd = None
		self.server_thread = None

	def start(self):
		self.httpd = HTTPServer((self.host, self.port), RequestHandler)
		self.server_thread = threading.Thread(target=self.httpd.serve_forever)
		self.server_thread.start()
		print(f"Server started on {self.host}:{self.port}")

	def stop(self):
		if self.httpd:
			self.httpd.shutdown()
			self.server_thread.join()
			print("Server stopped.")

server = Server()
server.start()

print ('after start')

time.sleep (2)

server.stop ()