


import threading
import time

# Function to be executed in a separate thread
def worker(stop_event):
    while not stop_event.is_set():
        print("Thread is doing some work...")
        time.sleep(1)
    print("Thread is stopping...")

# Create a threading Event to control the thread
stop_event = threading.Event()

# Start the thread
thread = threading.Thread(target=worker, args=(stop_event,))
thread.start()

# Main program logic continues here
print("Main program is running...")

# You can continue with the rest of your main program logic here

# To stop the thread, set the stop_event
stop_event.set()

# Wait for the thread to finish
thread.join()
print("Thread has finished.")
