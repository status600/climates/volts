






'''
	python3 /biotech/venues/warehouse/0_example/start.proc.py
'''

# cd /biotech/venues/warehouse/0_example


import pathlib
from os.path import dirname, join, normpath
import sys
def add_paths_to_system (paths):
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../stages_pip',
	'../../stages'
])

#
import pathlib
from os.path import dirname, join, normpath
#
import biotech
#
import rich



this_directory = pathlib.Path (__file__).parent.resolve ()
this_module = str (this_directory)

bio = biotech.on ({
	"glob_string": this_module + "/monitors/**/status_*.py",
	
	"simultaneous": True,
	"simultaneous_capacity": 10,

	"module_paths": [],

	"relative_path": this_module,
	
	"aggregation_format": 2
})

import time
time.sleep (20)

bio ["off"] ()

rich.print_json (data = bio ["proceeds"])

