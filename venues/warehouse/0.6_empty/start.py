


import threading
import time

def background_task():
	# This function performs the background task
	while True:
		print("Background task is running...")
		time.sleep(1)  # Adjust sleep duration as needed

def main():
	# Create and start the background thread
	background_thread = threading.Thread(target=background_task)
	background_thread.start ()

	# Keep the main thread alive indefinitely
	#while True:
	#    time.sleep(1)  # Adjust sleep duration as needed

if __name__ == "__main__":
	main()
	
	print ("here")