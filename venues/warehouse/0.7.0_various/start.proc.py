

'''
	python3 status.proc.py '_status/monitors/UT/6/status_1.py'
'''

import rich

import pathlib
from os.path import dirname, join, normpath
import json

import pathlib
from os.path import dirname, join, normpath
import sys
def add_paths_to_system (paths):
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../stages_pip',
	'../../stages'
])


import biotech

this_directory = pathlib.Path (__file__).parent.resolve ()
stasis = normpath (join (this_directory, "stasis"))

bio = biotech.on ({
	"glob_string": stasis + '/**/*_health.py',
	
	"simultaneous": True,
	"simultaneous_capacity": 1,

	"module_paths": [],

	"relative_path": stasis,
	
	"aggregation_format": 2
})

import time
time.sleep (20)

bio ["off"] ()

rich.print_json (data = bio ["proceeds"])


status = bio ["proceeds"] ["stats"]

print ("Unit test suite 6 status found:", json.dumps (status ["stats"], indent = 4))
assert (len (paths) == 3), paths
		
assert (status ["stats"]["paths"]["alarms"] == 1), status ["stats"]
assert (status ["stats"]["paths"]["empty"] == 1), status ["stats"]
assert (status ["stats"]["checks"]["passes"] == 7), status ["stats"]
assert (status ["stats"]["checks"]["alarms"] == 1), status ["stats"]
