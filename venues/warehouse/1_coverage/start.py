


'''

'''
def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../decor_pip',
	'../../decor'
])

import this_module
	

def generate_coverage (
	start = None
):
	import coverage
	cov = coverage.Coverage ()
	cov.start ()

	# .. call your code ..

	start ()

	cov.stop ()
	cov.save ()

	#cov.html_report ()
	cov.html_report (directory = "coverage_report")

	
generate_coverage (
	start = this_module.start
)