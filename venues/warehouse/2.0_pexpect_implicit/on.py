
'''
	python3 /biotech/venues/warehouse/2_pexpect/on.py
'''

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../stages_pip',
	'../../stages'
])

from biotech.topics.process_on.p_expect.implicit import process_on_implicit
from biotech.topics.implicit.thread import implicit_thread

import time
import sys
import os
import pathlib
from os.path import dirname, join, normpath
import signal
import threading

import rich

this_directory = pathlib.Path (__file__).parent.resolve ()
the_venture = str (normpath (join (this_directory, "venture.py")))

env = os.environ.copy ()
env ["PYTHONPATH"] = ":".join (sys.path)


print ("started??")


time.sleep (2)




		
	
def main ():
	venture = process_on_implicit (
		f"python3 '{ the_venture }'",
		env = env
	)

	def the_task (stop_event):
		nonlocal venture;
		#global venture;

		while not stop_event.is_set ():
			time.sleep (1)
			print ("status:", venture ["process"], venture ["process"].is_alive ())
			
		print ("task is off?")
		return;
		
	status_loop = implicit_thread (
		task = the_task
	)
	status_loop ['on'] ()
	time.sleep (3)
	status_loop ['off'] ()

	print ("after main?")
	venture ["process"].terminate ()

	the_records = venture ["records"] ()

	rich.print_json (data = {
		"records": the_records
	})
	
	
	for obj in the_records:
		print ("UTF8:", obj ["UTF8"] ["line"], end = '')

	return;

	time.sleep (2)

	#venture ["process"].send_signal(signal.SIGTERM)
	#venture ["process"].expect (pexpect.EOF)

	print ('offing')
	# venture ['off'] ()
	venture ["process"].terminate ()
	print ('offed')


	time.sleep (5)

	print ('turning the implicit task off.')
	status_loop ['off'] ()


	records = venture ["records"] ()
	print ("records:", records)

main ();