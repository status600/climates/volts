
'''
	python3 /biotech/venues/warehouse/2_pexpect/on.py
'''

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../stages_pip',
	'../../stages'
])

from biotech.topics.process_on.p_expect import process_on
from biotech.topics.implicit.thread import implicit_thread

import time
import sys
import os
import pathlib
from os.path import dirname, join, normpath
import signal
import threading

import rich

this_directory = pathlib.Path (__file__).parent.resolve ()
the_venture = str (normpath (join (this_directory, "venture.py")))

env = os.environ.copy ()
env ["PYTHONPATH"] = ":".join (sys.path)

ventures = {
	"1": None
}

def ventures_status (stop_event):
	global ventures;

	statuses = {}
	while not stop_event.is_set ():
		time.sleep (1)
		print ("status:", ventures)
		
		for venture in ventures:
			try:
				if (ventures [venture] ["process"].is_alive ()):
					status = "on"
				else:
					status = "off"
			
				statuses [ venture ] = {
					"status": status
				}
			
			except Exception as E:
				print (ventures)
				pass;
		
		rich.print_json (data = statuses)
	
	print ("thread is done")
	return;

		
	
def main ():
	status_loop = implicit_thread (
		task = ventures_status
	)
	status_loop ['on'] ()
	
	
	ventures ["1"] = process_on (
		f"python3 '{ the_venture }'",
		env = env
	)

	time.sleep (4)

	ventures ["1"] ["process"].terminate ()
	
	#----
	#
	the_records = ventures ["1"] ["records"] ()
	rich.print_json (data = {
		"records": the_records
	})	
	for obj in the_records:
		print ("UTF8:", obj ["UTF8"] ["line"], end = '')

	#
	#
	#----
	
	time.sleep (3)
	status_loop ['off'] ()

main ();