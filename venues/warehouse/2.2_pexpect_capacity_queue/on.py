
'''
	python3 /biotech/venues/warehouse/2_pexpect/on.py
'''

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../stages_pip',
	'../../stages'
])

from biotech.topics.process_on.p_expect.implicit import process_on_implicit
from biotech.topics.process_on.p_expect import process_on

from biotech.topics.implicit.thread import implicit_thread
from biotech.topics.queues.queue_capacity_limiter import queue_capacity_limiter

import time
import sys
import os
import pathlib
from os.path import dirname, join, normpath
import signal
import threading

import rich

this_directory = pathlib.Path (__file__).parent.resolve ()
the_venture_path = str (normpath (join (this_directory, "venture.py")))

env = os.environ.copy ()
env ["PYTHONPATH"] = ":".join (sys.path)


print ("started??")


ventures = {}

def venture (process_path):
	print ("venture:", process_path)
	
	ventures ["1"] = process_on_implicit (
		f"python3 '{ process_path }'",
		env = env
	)
	
	'''
	ventures ["1"] = process_on (
		f"python3 '{ process_path }'",
		env = env
	)
	'''
	
	return ventures ["1"]


def the_task (stop_event):
	global ventures;

	while not stop_event.is_set ():
		time.sleep (1)
		
		print ("the task loop")
		
		#print ("status:", venture ["process"], venture ["process"].is_alive ())
		#print ('status:', ventures)
		
		for venture_path in ventures:
			print ('status:', ventures [ venture_path ] ["process"].is_alive ())
		
		
	print ("task is off?")
	return;

def main ():
	status_loop = implicit_thread (
		task = the_task
	)
	status_loop ['on'] ()
	
	proceeds = queue_capacity_limiter (
		capacity = 1,
		items = [
			str (normpath (join (this_directory, "venture.py")))
		],
		move = venture
	)
	
	print ("proceeds:", proceeds)
	
	
	
	time.sleep (3)
	proceeds [0] ["process"].terminate ()
	time.sleep (3)
	
	status_loop ['off'] ()

	print ("after main?")
	
	'''
	

	the_records = venture ["records"] ()

	rich.print_json (data = {
		"records": the_records
	})
	
	
	for obj in the_records:
		print ("UTF8:", obj ["UTF8"] ["line"], end = '')
	'''
	

main ();