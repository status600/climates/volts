

print ('script')


import time
import traceback
from quart import Quart, render_template, websocket	
	
		
	
try:
	app = Quart(__name__)

	


	@app.route("/")
	async def hello ():
		return "Hi!"
	

	@app.route("/api")
	async def json ():
		return {
			"hello": "world"
		}
		

	@app.websocket ("/ws")
	async def ws ():
		while True:
			await websocket.send ("hello")
			await websocket.send_json ({
				"hello": "world"
			})
			
	if __name__ == "__main__":
		app.run (
			host = '0.0.0.0'
		)
		print ('in main')

		
except Exception as E:
	print ("venture exception:", traceback.format_exc ())


while True:
	print ("here")
	time.sleep (1)