
'''
	python3 /biotech/venues/warehouse/0_example/start.proc.py
'''


def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../stages_pip',
	'../../stages'
])

import pathlib
from os.path import dirname, join, normpath
this_directory = pathlib.Path (__file__).parent.resolve ()
this_module = str (this_directory)

DB_direcotry = str (normpath (join (this_directory, "DB")))

import biotech
scan = biotech.start (
	glob_string = this_module + "/**/status_*.py",
	
	#
	#	This runs all the checks in a thread pool,
	#	so maybe at the same time, more or less.
	#
	simultaneous = True,
	
	#
	#	This is the upper limit for the number
	#	of processes that can be in progress
	#	at once.
	#
	simultaneous_capacity = 10,
	
	#
	#	If the process requires locally installed
	#	modules in custom places.
	#
	module_paths = [
		normpath (join (this_module, "modules")),
		normpath (join (this_module, "modules_pip"))
	],
	
	#
	#	This removes the substring provided
	#	from the output.
	#
	#	For example:
	#		if 
	#			"this_module" = /modules/this_module/1"
	#			and the status file path = /modules/this_module/1/_status/status_1.py
	#
	#		then, 
	#			the path shown in the output is "_status/status_1.py"
	#
	relative_path = this_module,
	
	#
	#	This saves the proceeds of the scan (with tinydb)
	#
	db_directory = DB_direcotry,
	
	aggregation_format = 2
)