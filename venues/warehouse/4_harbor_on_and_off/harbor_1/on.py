
from biotech.topics.process_on.p_expect.implicit import process_on_implicit
import json
import pathlib
import os
from os.path import dirname, join, normpath
import sys
from multiprocessing import Process
import threading
import time

def the_intro_process_path ():
	this_folder = pathlib.Path (__file__).parent.resolve ()
	return str (normpath (join (this_folder, "start.proc.py")))

def turn_on ():
	process_environment = os.environ.copy ()
	process_environment ["PYTHONPATH"] = ":".join ([
		* sys.path
	])
	
	the_intro = process_on_implicit (
		"python3 " + the_intro_process_path (),
		
		env = process_environment,
		name = "intro"
	)
	
	return the_intro;