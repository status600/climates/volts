






'''
	python3 /biotech/venues/warehouse/0_example/start.proc.py
'''

# cd /biotech/venues/warehouse/0_example


import pathlib
from os.path import dirname, join, normpath
import sys
def add_paths_to_system (paths):
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../stages_pip',
	'../../stages'
])

print ("harbor on")

import time

from harbor_1.on import turn_on
venture = turn_on ()


print ("harbor on?")

time.sleep (2)
print ("turning off")
venture ["process"].terminate ()

time.sleep (1)

venture_2 = turn_on ()
print ("turning on again")

time.sleep (2)
