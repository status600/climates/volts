





import rich

import pathlib
from os.path import dirname, join, normpath
import json
import time

import pathlib
from os.path import dirname, join, normpath
import sys

this_folder = pathlib.Path (__file__).parent.resolve ()	

def add_paths_to_system (paths):
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../stages_pip',
	'../../stages'
])


import subprocess
import threading

def start_background_process(command, bufsize=None, stdout_handler=None, stderr_handler=None):
    # Start the process in the background and specify buffer size
    process = subprocess.Popen(command, shell=True, bufsize=bufsize, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    
    # Create a thread to handle stdout
    if stdout_handler:
        stdout_thread = threading.Thread(target=stdout_handler, args=(process.stdout,))
        stdout_thread.start()
    
    # Create a thread to handle stderr
    if stderr_handler:
        stderr_thread = threading.Thread(target=stderr_handler, args=(process.stderr,))
        stderr_thread.start()
    
    return process

# Example stdout handler function
def handle_stdout(stdout):
    for line in iter(stdout.readline, b''):
        print("STDOUT:", line.decode('utf-8').strip())

# Example stderr handler function
def handle_stderr(stderr):
    for line in iter(stderr.readline, b''):
        print("STDERR:", line.decode('utf-8').strip())

# Example usage
command = 'python3 /biotech/venues/warehouse/5_implicit_process/process_1_USD.py'

# Start the background process with handlers for stdout and stderr
process = start_background_process(command, bufsize=0, stdout_handler=handle_stdout, stderr_handler=handle_stderr)

# Optionally, you can wait for the process to finish
# process.wait()

# Stop the background process

print ("here")

import time

while True:
	time.sleep (1)
	
	
process.terminate()