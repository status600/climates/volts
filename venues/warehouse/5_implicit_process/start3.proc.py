

import rich

import pathlib
from os.path import dirname, join, normpath
import json
import time

import pathlib
from os.path import dirname, join, normpath
import sys

this_folder = pathlib.Path (__file__).parent.resolve ()	

def add_paths_to_system (paths):
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../stages_pip',
	'../../stages'
])


from biotech.topics.process_on.p_expect.implicit import process_on_implicit
	
the_process = str (normpath (join (this_folder, 'process_1_USD.py')))	
	
venture = process_on_implicit (
	'python3 ' + the_process,
	
	CWD = None,
	env = {}
)


#
# status
#
#
print ('is going:', venture ["is_going"] ())

#
#	stop the process
#
#


#records = venture ["records"] ()

time.sleep (2)

venture ["stop"] ()

print ('is going:', venture ["is_going"] ())


