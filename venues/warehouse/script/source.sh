#!/bin/sh


#
#	https://unix.stackexchange.com/questions/76505/unix-portable-way-to-get-scripts-absolute-path-in-zsh
#
this_directory=${0:a:h}
echo this_directory


export PATH=$PATH:$this_directory/bin

echo $HERE